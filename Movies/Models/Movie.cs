﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;

namespace Movies.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Год")]
        public int Year { get; set; }

        [Required]
        [Display(Name = "Режисер")]
        public string Producer { get; set; }

        //[Required]
        [Display(Name = "Постер")]
        public byte[] Poster { get; set; }

        [Required]
        [Display(Name = "Автор")]
        public string CurrentUser { get; set; }
    }
}